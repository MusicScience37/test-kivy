# test-kivy

GUI ライブラリ Kivy の練習をするためのリポジトリ

## ディレクトリ

| ディレクトリ | 内容                                                             |
| :----------- | :--------------------------------------------------------------- |
| basic        | [Kivy Basics の例](https://kivy.org/doc/stable/guide/basic.html) |
| basic2       | [Kivy Basics の例](https://kivy.org/doc/stable/guide/basic.html) |
